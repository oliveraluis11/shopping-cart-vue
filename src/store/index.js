import { createStore } from 'vuex'

export default createStore({
  state: {
    productos:[],
    carrito:{}
  },
  mutations: {
    setProductos(state, payload){
      state.productos = payload
      console.log(state.productos)
    },
    setCarrito(state,payload){
      state.carrito[payload.id] = payload
    },
    vaciarCarrito(state){
      state.carrito ={}
    },
    aumentarItem(state, payload){
      state.carrito[payload].cantidad += 1
    },
    disminuirItem(state,payload){
      state.carrito[payload].cantidad -=1
      if (state.carrito[payload].cantidad ===0) delete state.carrito[payload]
      
    }
  },
  actions: {
    async fetchData({commit}){
      try{
        const res= await fetch('api.json')
        const data = await res.json()
        commit('setProductos',data)
      }catch(error){
        console.log(error)
      }
    },
    agregarAlCarro({commit,state}, producto){
      state.carrito.hasOwnProperty(producto.id)
        ? producto.cantidad = state.carrito[producto.id].cantidad + 1
        : producto.cantidad = 1
      commit('setCarrito',producto)
    },
    eliminarAlCarro({commit, state},producto){
      let i = 0
      if(state.carrito[producto.id].cantidad != 0){
        producto.cantidad = state.carrito[producto.id].cantidad -1
      } else {
        i =state.carrito.indexOf(producto)
      }
      
         
    }
  },
  getters:{
    totalCantidad(state){
      return Object.values(state.carrito).reduce((acc,{cantidad})=>acc + cantidad,0)
    },
    totalPrecio(state){
      return Object.values(state.carrito).reduce((acc,{cantidad,precio})=>acc + cantidad*precio,0)
    }
  },
  modules: {
  }
})
